import java.util.*;

public class Test {
	public static void main(String [] args){
		Scanner in = new Scanner(System.in);
		System.out.println("Enter company name (Kia or Audi):");
		String company = in.next();
		String model = "";
		if (company.equals("Kia")){
			System.out.println("Enter model (Rio, Cerato, Optima):");
			model = in.next();
		}
		else if (company.equals("Audi")){
			System.out.println("Enter model (Roadster, Cabriolet, Coupe):");
			model = in.next();
		}
		System.out.println("Enter any car color:");
		String color = in.next();
		System.out.println("Enter any car engine volume:");
		double volume = in.nextDouble();

		AbstractFactory factory = FactoryProducer.getFactory(company);
		CarBuilder carBuilder = factory.getCarBuilder(model);
		Engineer eng = new Engineer(carBuilder);
		eng.constructCar();
		Car car = eng.getCar();
		car.setColor(color);
		car.setEngineVolume(volume);


		System.out.println("Car is builded: " + car);
	}
}