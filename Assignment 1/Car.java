public class Car {
	private double engineVolume;
	private String color;
	private String company;
	private String model;
	public void setEngineVolume(double volume){
		engineVolume = volume;
	}
	public void setColor(String color){
		this.color = color;
	}
	public void setCompany(String company){
		this.company = company;
	}
	public void setModel(String model){
		this.model = model;
	}
	@Override
	public String toString(){
		return company + ", " + model + ", color: " + color + ", engine volume: " + engineVolume;
	}
}

interface CarBuilder {
	public void buildCompany();
	public void buildModel();
	public Car getCar();
}