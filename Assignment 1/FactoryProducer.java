public class FactoryProducer {
	public static AbstractFactory getFactory(String type){
		if (type.equalsIgnoreCase("Audi"))
			return new AudiFactory();
		else if (type.equalsIgnoreCase("Kia"))
			return new KiaFactory();
		return null;
	}
}