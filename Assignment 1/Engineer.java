public class Engineer {

	private CarBuilder carBuilder;

	public Engineer(CarBuilder carBuilder){
		this.carBuilder = carBuilder;
	}

	public Car getCar(){
		return carBuilder.getCar();
	}

	public void constructCar(){
		this.carBuilder.buildModel();
		this.carBuilder.buildCompany();
	}
}